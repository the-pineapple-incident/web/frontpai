import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: '10rem',
  },
  wrapper: {
    position: 'relative',
    minHeight: '100vh',
  },
}));

export default useStyles;
