import React from 'react';
import { useQuery } from '@apollo/client';
import CircularProgress from '@material-ui/core/CircularProgress';
import Footer from './components/Footer';
import NavBar from './components/NavBar';
import { navList } from './navList';
import useStyles from './styles';
import getAllActivePoliticalEvents from '../../services/political-event/query';

const MainLayout = (props) => {
  const { children } = props;
  const classes = useStyles();
  const { loading,
    error,
    data } = useQuery(getAllActivePoliticalEvents);
  if (loading) {
    return (
      <CircularProgress />
    );
  };
  if (!error) {

    data.activePoliticalEvents?.map(
      (ev) => {
        if (ev.name.includes('CONGRESALES')) { localStorage.setItem('idCongresales', ev.id); } else { localStorage.setItem('idPresidenciales', ev.id); }
        return '';
      },
    );

  }

  return (
    <div className={classes.wrapper}>
      <NavBar navList={navList} />
      <div className={classes.container}>
        {children}
      </div>
      <Footer />
    </div>
  );
};

export default MainLayout;
