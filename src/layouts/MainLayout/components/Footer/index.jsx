import React from 'react';
import { Typography } from '@material-ui/core';
import useStyles from './styles';

const Footer = (props) => {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Typography variant="h6">Pineapple Incident</Typography>
    </footer>
  );
};

export default Footer;
