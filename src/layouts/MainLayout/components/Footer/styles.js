import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  footer: {
    position: 'absolute',
    bottom: 0,
    width: '100% !important',
    height: '80px',
    background: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
}));

export default useStyles;
