import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { List, Toolbar, Typography, Box, IconButton, Menu, MenuItem } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import ListItemLink from '../ListItemLink';
import useStyles from './styles';

const NavBar = (props) => {
  const { navList = [] } = props;
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <AppBar position="static" color="primary">
      <Toolbar>
        <Box display={{ lg: 'none', xl: 'none' }}>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="simple-menu" onClick={handleClick}>
            <MenuIcon />
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            {
              navList.map((item) => (
                <MenuItem key={item.id} component={Link} to={item.path}>
                  {item.name}
                </MenuItem>
              ))
            }
          </Menu>
        </Box>
        <Typography variant="h6" className={classes.title}>
          Elige Informado
        </Typography>
        <Box display={{ xs: 'none', sm: 'none', md: 'none', lg: 'block', xl: 'block' }}>
          <List className={classes.listContainer}>
            {
              navList.map((item) => (
                <ListItemLink key={item.id} index={item.id} to={item.path} primary={item.name} />
              ))
            }
          </List>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
