import React from 'react';
import { Box } from '@material-ui/core';
import useStyles from './styles';

const BoxStyled = (props) => {
  const classes = useStyles();
  const { children } = props;
  return (
    <Box className={classes.BoxStyle} display={{ xs: 'none', sm: 'none', md: 'none', lg: 'block', xl: 'block' }}>

      {children}

    </Box>
  );
};

export default BoxStyled;
