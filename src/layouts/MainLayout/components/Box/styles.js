import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  BoxStyle: {
    borderColor: theme.palette.secondary.main,
    borderWidth: `${1}px`,
    borderStyle: 'solid',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '98%',
    position: 'relative',
    height: '55vh',
  },
}));

export default useStyles;
