import React from 'react';
import { List, ListItem, ListItemText, Box, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { Link } from 'react-router-dom';
import useStyles from './styles';
//import ListItemLink from '../ListItemLink';
const NavTabs = (props) => {
  const classes = useStyles();
  const { navList = [] } = props;
  const [information, setInformation] = React.useState('Información Política');

  const phoneBoxDefault = {
    display: { lg: 'none', xl: 'none' },
  };
  const webBoxDefault = {
    display: { xs: 'none', sm: 'none', md: 'none', lg: 'block', xl: 'block' },
  };

  const handleChange = (event) => {
    setInformation(event.target.value);
  };
  return (
    <div>
      <Box {...phoneBoxDefault}>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="navtab-simple-select-outlined-label">Tema</InputLabel>
          <Select
            labelId="navtab-simple-select-outlined-label"
            id="navtab-simple-select-outlined"
            value={information}
            onChange={handleChange}
            label="Information"
          >
            {
              navList.map((item) => (
                <MenuItem key={item.id} value={item.name} component={Link} to={item.path}>
                  {item.name}
                </MenuItem>
              ))
            }
          </Select>
        </FormControl>
      </Box>
      <Box {...webBoxDefault}>
        <List className={classes.listContainer}>
          {
            navList.map((item) => (
              <ListItem button key={item.id} component={Link} to={item.path} className={classes.slink}>
                <ListItemText primary={item.name} />
              </ListItem>
            ))
          }
        </List>
      </Box>
    </div>
  );
};

export default NavTabs;

