import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  slink: {
    borderColor: theme.palette.secondary.main,
    backgroundColor: theme.palette.secondary.main,
    borderWidth: `${1}px`,
    borderStyle: 'solid',
    borderTopLeftRadius: `${20}px`,
    borderTopRightRadius: `${20}px`,
    color: theme.palette.secondary.contrastText,
    width: 'auto',
  },
  listContainer: {
    display: 'flex',
    flexDirection: 'row',
    paddingBottom: 0,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));
export default useStyles;
