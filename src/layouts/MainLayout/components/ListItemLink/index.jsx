import React from 'react';
import { ListItemIcon, ListItemText, ListItem } from '@material-ui/core';
import { NavLink as RouterLink } from 'react-router-dom';

function ListItemLink(props) {
  const { icon, primary, to } = props;
  const activeStyle = { borderBottom: 'solid 3px #fff', paddingBottom: '1em' };

  const renderLink = React.useMemo(
    () => React.forwardRef((itemProps, ref) => (
      <RouterLink
        to={to}
        ref={ref}
        activeStyle={activeStyle}
        {...itemProps}
      />
    )),
    [to],
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
}

export default ListItemLink;
