import * as ROUTES from '../../routers/routes';

export const navList = [
  { id: 1, name: 'Figuras políticas', path: ROUTES.POLITICAL_FIGURES },
  { id: 2, name: 'Elecciones presidenciales', path: ROUTES.HOME_PAGE },
  {
    id: 3,
    name: 'Elecciones Congresales',
    path: ROUTES.CONGRESSIONAL_ELECTIONS,
  },
];

export const navList2 = [
  { id: 1, name: 'Información Política', path: ROUTES.HOME_PAGE },
  { id: 2, name: 'Eventos', path: ROUTES.HOME_PAGE + ROUTES.EVENT_TOPIC },
  {
    id: 3,
    name: 'Análisis de Redes Sociales',
    path: ROUTES.HOME_PAGE + ROUTES.SOCIAL_MEDIA_ANALYSIS,
  },
  { id: 4, name: 'Tópicos', path: ROUTES.HOME_PAGE + ROUTES.DIMENSION },
];

export const navList3 = [
  { id: 1, name: 'Información Política', path: ROUTES.CONGRESSIONAL_ELECTIONS },
  {
    id: 2,
    name: 'Eventos',
    path: ROUTES.CONGRESSIONAL_ELECTIONS + ROUTES.EVENT_TOPIC,
  },
  {
    id: 3,
    name: 'Análisis de Redes Sociales',
    path: ROUTES.CONGRESSIONAL_ELECTIONS + ROUTES.SOCIAL_MEDIA_ANALYSIS,
  },
  {
    id: 4,
    name: 'Tópicos',
    path: ROUTES.CONGRESSIONAL_ELECTIONS + ROUTES.DIMENSION,
  },
];
