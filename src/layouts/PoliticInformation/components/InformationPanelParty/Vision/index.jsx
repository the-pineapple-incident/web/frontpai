import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import useStyles from '../styles';

const VisionInformation = (props) => {
  const socialInfo = [
    { Info: 'Ideas generales:',
      Description:
      'La visión que hemos recogido de todos los peruanos, busca un país seguro donde todos vivimos en armonía entre nosotros, con nuestras comunidades y el medio ambiente; con crecimiento económico que beneficie a todos los peruanos, que premia su esfuerzo y que busca hacer realidad sus sueños de prosperidad. Nuestro Plan de Gobierno intenta reflejar las ideas y necesidades de todos los peruanos, para lo cual está organizado en seis pilares, atendiendo a las recomendaciones del Jurado Nacional de Eleccione y que creemos responden a los principales retos del país e incluyen las instituciones que debemos poner en operación para que la prosperidad y las oportunidades lleguen a todos los peruanos. I. Primer Bloque: DERECHOS FUNDAMENTALES Y DIGNIDAD DE LAS PERSONAS Fortaleciendo las Instituciones del Estado de Derecho, la Justicia y los Derechos Humanos, donde consolidamos un verdadero balance e independencia de los Poderes del Estado y una efectiva protección a los derechos de las personas. II. Segundo Bloque: OPORTUNIDADES Y ACCESO A LOS SERVICIOS Creando un Estado que responde a las necesidades de los ciudadanos, donde todos los peruanos tenemos acceso a servicios de calidad y la oportunidad de prosperar. III. Tercer Bloque: ESTADO GOBERNABILIDAD Recuperando un País Seguro, para vivir Con Paz y Sin Miedo. Creando un Estado Eficiente, que responde a las necesidades de los peruanos y les rinde cuentas por sus actos.' }];
  const classes = useStyles();
  return (
    <Grid
      container
      className={classes.generalInformation}
      direction="column"
      justify="flex-start"
      alignItems="flex-start"
      xs={12}
      sm={12}
    >
      <Typography variant="h5">Visión</Typography>
      <br />
      {socialInfo.map((socialItem) => (
        <div>
          <Box>
            <ul>
              <li>
                <Typography variant="body1">
                  {socialItem.Info}
                </Typography>
                <Typography align="justify" paragraph variant="body2">
                  {socialItem.Description}
                </Typography>
              </li>
            </ul>
          </Box>
        </div>
      ))}
    </Grid>
  );
};

export default VisionInformation;
