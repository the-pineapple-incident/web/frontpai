import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  generalInformation: {
    width: '-webkit-fill-available',
    borderWidth: '10px solid',
    borderColor: theme.palette.primary.main,
    borderStyle: 'solid',
    padding: '15px',
    marginLeft: '70px',
    [theme.breakpoints.down('sm')]: {
      margin: '0',
      padding: '10px',
      width: '65vw',
    },
  },
}));
export default useStyles;
