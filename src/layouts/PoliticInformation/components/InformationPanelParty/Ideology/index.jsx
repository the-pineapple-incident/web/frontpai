import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import useStyles from '../styles';

const IdeologyInformation = (props) => {
  const socialInfo = [
    { Info: 'Principios, objetivos y valores de la organización política:',
      Description: 'El Fujimorismo es una fuerza popular que surge en el escenario político en una de las etapas históricas más difíciles que ha enfrentado nuestro país en su etapa republicana, asolado por el fenómeno sin precedentes del terrorismo, así como por una hiperinflación solo comparable al desastre económico tras la debacle de la Guerra del Pacífico. Así, el Fujimorismo se forja en condiciones excepcionalmente difíciles y construye sus principios y objetivos en una práctica de gobierno que conduce a la victoria al derrotar los dos problemas más graves de nuestra historia republicana. El Fujimorismo es una fuerza popular identificada con los más pobres y los peruanos históricamente postergados por el Estado que define y orienta el desarrollo del Perú con justicia social, con equidad, con derecho al progreso y a la seguridad personal en igualdad de condiciones y de oportunidades.' }];
  const classes = useStyles();
  return (
    <Grid
      container
      className={classes.generalInformation}
      direction="column"
      justify="flex-start"
      alignItems="flex-start"
      xs={12}
      sm={12}
    >
      <Typography variant="h5">Ideario</Typography>
      <br />
      {socialInfo.map((socialItem) => (
        <div>
          <Box>
            <ul>
              <li>
                <Typography variant="body1">
                  {socialItem.Info}
                </Typography>
                <Typography align="justify" paragraph variant="body2">
                  {socialItem.Description}
                </Typography>
              </li>
            </ul>
          </Box>
        </div>
      ))}
    </Grid>
  );
};

export default IdeologyInformation;
