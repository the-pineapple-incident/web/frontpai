import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import useStyles from '../styles';

const SocialDimension = (props) => {
  const socialInfo = [
    { Info: 'Problemas identificados:',
      Description: 'Los últimos 15 años ha habido importantes avances en la cobertura de matrícula en los tres niveles de la Educación Básica Regular. Sin embargo, no todos los estudiantes terminan la escuela. La tasa de no conclusión de primaria es de 15 % y en secundaria sube al 30 %. Pero si vemos los índices de nuestra población afectada por pobreza extrema y que vive en áreas rurales la tasa de no conclusión de primaria se eleva al 40% y la de secundaria a 60 %, según cifras del Ministerio de Educación. Si bien a nivel de logros de aprendizajes evaluados (ECE 2014) hemos registrado avances, con un 43,5% de estudiantes que comprenden satisfactoriamente lo que leen, solo un 25,9% es capaz de resolver bien problemas matemáticos. ' },
    { Info: 'Objetivos Estratégicos:',
      Description: ' I. Garantizar aprendizajes útiles y que contribuyan al desarrollo de las personas, sus talentos y capacidades. a. Fortaleciendo la educación infantil: • Aumentar la inversión y ejecución presupuestal en la primera infancia desde un enfoque multisectorial (Salud, Educación, Identidad y Justicia), fortaleciendo al ciudadano desde la primera infancia. • Lograr cobertura plena en 3,4 y 5 años de edad. • Diseñar e implementar distintos modelos de educación inicial adaptados a la diversidad del país. • Reconvertir la totalidad de los PRONOEIs en Centros de Educación Inicial iniciando desde las zonas rurales, hacia las zonas urbanas. ' },
    { Info: 'Indicadores:',
      Description: '- Incremento de la Matrícula en la educación pre escolar. - Mejora progresiva en los indicadores de aprendizajes.' },
    { Info: 'Metas:',
      Description: '- 30% de incremento de la matrícula en educación pre escolar. - 10% de incremento anual en los indicadores de aprendizaje.' },
  ];
  const classes = useStyles();
  return (
    <Grid
      container
      className={classes.generalInformation}
      direction="column"
      justify="flex-start"
      alignItems="flex-start"
      xs={12}
      sm={12}
    >
      <Typography variant="h5">Dimensión Social</Typography>
      <br />
      {socialInfo.map((socialItem) => (
        <div>
          <Box>
            <ul>
              <li>
                <Typography variant="body1">
                  {socialItem.Info}
                </Typography>
                <Typography align="justify" paragraph variant="body2">
                  {socialItem.Description}
                </Typography>
              </li>
            </ul>
          </Box>
        </div>
      ))}
    </Grid>
  );
};

export default SocialDimension;
