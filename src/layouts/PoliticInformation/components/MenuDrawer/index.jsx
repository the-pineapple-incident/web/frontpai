import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import TitlePanel from '../TitlePanel';
import InformationPanel from '../InformationPanel/generalInformation';
import useStyles from './styles';

function MenuDrawer(props) {
  const classes = useStyles();

  const drawer = (
    <div>
      <List>
        {['Datos personales', 'Experiencia', 'Educación', 'Bienes'].map((text) => (
          <ListItem button key={text}>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Typography paragraph>Pequeño</Typography>
        </Hidden>
        <Hidden xsDown implementation="css">

          {drawer}

        </Hidden>
      </nav>
      <main className={classes.content}>
        <TitlePanel
          imgCandidato=""
          nombreCandidato="Yonhy Lescano Ancieta"
          nombrePartido="Acción Popular"
          imgPartido=""
        />
        <InformationPanel>
          {props}
        </InformationPanel>
      </main>
    </div>
  );
}

export default MenuDrawer;
