import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import InformationBox from '../utils/InformationBox';
import useStyles from '../utils/styles';

const InformationPanel = (props) => {
  const { title, informationList = [] } = props;

  //console.log(informationList);
  const classes = useStyles();

  const Rlist = informationList.filter((item) => item.id.includes('R'));
  const Llist = informationList.filter((item) => !item.id.includes('R'));

  const width = Rlist.length === 0 ? 12 : 6;
  const styleR = Rlist.length === 0 ? { display: 'none' } : {};

  return (
    <div className={classes.generalInformation}>
      <Typography variant="h5">
        { title }
      </Typography>
      <Grid
        className={classes.information}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Grid
          className={classes.info}
          container
          item
          xs={width}
        >
          {Llist.map((item) => (
            <InformationBox key={item.id} info={item.info}>
              {item.content}
            </InformationBox>
          ))}
        </Grid>
        <Grid
          className={classes.info}
          container
          item
          xs={6}
          style={styleR}
        >
          {Rlist.map((item) => (
            <InformationBox key={item.id} info={item.info}>
              {item.content}
            </InformationBox>
          ))}
        </Grid>
      </Grid>
    </div>
  );
};

export default InformationPanel;
