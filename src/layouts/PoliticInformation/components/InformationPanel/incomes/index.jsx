import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(description, privAmount, pubAmount, totAmount) {
  return { description, privAmount, pubAmount, totAmount };
}

const rows = [
  createData('Ingreso Neto', 2000, 2000, 4000),
  createData('Renta', 2000, 2000, 4000),
  createData('Otros', 2000, 2000, 4000),
  createData('Subtotal', 6000, 6000, 12000),
];

const Incomes = (props) => {
  const classes = useStyles();
  return (
    <div>
      <h1>Ingresos del año </h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} size="medium" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Descripción</TableCell>
              <TableCell>Privado</TableCell>
              <TableCell>Público</TableCell>
              <TableCell>Total</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow>
                <TableCell component="th" scope="row">
                  {row.description}
                </TableCell>
                <TableCell>{row.privAmount}</TableCell>
                <TableCell>{row.pubAmount}</TableCell>
                <TableCell>{row.totAmount}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default Incomes;
