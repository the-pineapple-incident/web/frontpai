import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import useStyles from './styles';

const InformationBox = (props) => {
  const { info, children } = props;
  const classes = useStyles();
  return (
    <Grid
      container
      item
      direction="row"
      justify="space-between"
      alignItems="center"
    >
      <Grid item xs={5}>
        <Typography className={classes.leftInfo} variant="body1">
          <b>
            {info}
          </b>
        </Typography>
      </Grid>
      <Grid item xs={5}>
        <Typography className={classes.rightInfo} variant="body1">
          {children}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default InformationBox;
