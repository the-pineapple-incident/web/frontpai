import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  leftInfo: {
    marginLeft: '1vw',
    [theme.breakpoints.down('sm')]: {
      marginLeft: '0',
    },
  },
  rightInfo: {
    marginLeft: 'auto',
  },
}));

export default useStyles;
