import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  generalInformation: {
    width: '-webkit-fill-available',
    maxWidth: '800px',
    borderWidth: '10px solid',
    borderColor: theme.palette.primary.main,
    borderStyle: 'solid',
    marginTop: '0px',
    marginLeft: '70px',
    padding: '15px',
    [theme.breakpoints.down('sm')]: {
      borderColor: theme.palette.primary.main,
      margin: '0',
      padding: '10px',
      width: '65vw',
    },
  },
  information: {
    flexDirection: 'row',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  info: {
    maxWidth: '100%',
  },
  itemListedInfo: {
    maxWidth: '100%',
    minWidth: '30vw',
  },
}));

export default useStyles;
