import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import InformationBox from '../utils/InformationBox';
import useStyles from '../utils/styles';

const EducationPanel = (props) => {
  const [educationInfo] = React.useState([
    { level: 'PRIMARIA', completed: false, institution: 'Institucion Educativa María Auxiliadora', specialty: '', graduateYear: '2000' },
    { level: 'SECUNDARIA', completed: true, institution: 'Institucion Educativa María Auxiliadora', specialty: '', graduateYear: '2005' },
    { level: 'UNIVERSITARIA', completed: true, institution: 'Pontificia Universidad Católica del Perú', specialty: 'Derecho', graduateYear: '2010' },
    { level: 'POSGRADO', completed: true, institution: 'Pontificia Universidad Católica del Perú', specialty: 'Derechos Humanos', graduateYear: '2015' },
  ]);

  const classes = useStyles();
  return (
    <div className={classes.generalInformation}>
      <Typography variant="h5"> Educación </Typography>
      <br />
      <Grid
        className={classes.information}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        {educationInfo.map((educationItem) => (
          <div>
            <Box>
              <Grid
                className={classes.itemListedInfo}
                container
              >
                <ul>
                  <li>
                    <InformationBox info="Nivel:">
                      {educationItem.level}
                    </InformationBox>
                    <InformationBox info="Completado:">
                      {educationItem.completed ? 'Sí' : 'No'}
                    </InformationBox>
                    <InformationBox info="Institución:">
                      {educationItem.institution}
                    </InformationBox>
                    <InformationBox info="Especialidad:">
                      {educationItem.specialty === '' ? 'No aplica' : educationItem.specialty}
                    </InformationBox>
                    <InformationBox info="Año de Graduación:">
                      {educationItem.graduateYear}
                    </InformationBox>
                  </li>
                </ul>
              </Grid>
            </Box>
          </div>
        ))}
      </Grid>
    </div>
  );
};

export default EducationPanel;
