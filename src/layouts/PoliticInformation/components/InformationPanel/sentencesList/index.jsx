import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(record, sentenceDate, judcialAuthority, penalCrime, verdict) {
  return { record, sentenceDate, judcialAuthority, penalCrime, verdict };
}

const rows = [
  createData('123456', 2020, 'Supremo', 'Juzgado de paz letrado', 'Civil'),
  createData('789111', 2019, 'Superior', 'Juzgado mixto', 'Familia civil'),
  createData('745896', 2018, 'Superior', 'Sala superior', 'Familia tutelar'),
  createData('789525', 2018, 'Superior', 'Sala superior', 'Laboral'),
  createData('456132', 2017, 'Superior', 'Sala superior', 'Laboral'),
];

const SentencesPanel = (props) => {
  const classes = useStyles();
  return (
    <div>
      <h1>Sentencias Penales</h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} size="medium" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>N° Registro</TableCell>
              <TableCell>Fecha de sentencia</TableCell>
              <TableCell>Autoridad responsable</TableCell>
              <TableCell>Descripción</TableCell>
              <TableCell>Veredicto</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.record}>
                <TableCell component="th" scope="row">
                  {row.record}
                </TableCell>
                <TableCell>{row.sentenceDate}</TableCell>
                <TableCell>{row.judcialAuthority}</TableCell>
                <TableCell>{row.penalCrime}</TableCell>
                <TableCell>{row.verdict}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <h1>Sentencias Civiles</h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} size="medium" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>N° Registro</TableCell>
              <TableCell>Autoridad responsable</TableCell>
              <TableCell>Descripción</TableCell>
              <TableCell>Veredicto</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.record}>
                <TableCell component="th" scope="row">
                  {row.record}
                </TableCell>
                <TableCell>{row.judicialAuthority}</TableCell>
                <TableCell>{row.sentence}</TableCell>
                <TableCell>{row.verdict}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default SentencesPanel;
