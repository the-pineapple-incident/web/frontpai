import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import InformationBox from '../../utils/InformationBox';
import useStyles from '../../utils/styles';

const ImmovablesPanel = (props) => {
  const [immovables] = React.useState([
    { isInSunarp: true, sunarpRecord: '12353242', valuation: 15000.4 },
    { isInSunarp: false, sunarpRecord: '', valuation: 1200.422 },
    { isInSunarp: true, sunarpRecord: '12353242', valuation: 11002.5 },
    { isInSunarp: false, sunarpRecord: '', valuation: 17820.65 },
    { isInSunarp: true, sunarpRecord: '12353242', valuation: 110340.99 },
  ]);

  const classes = useStyles();

  return (
    <div className={classes.generalInformation}>
      <Typography variant="h5"> Experiencia Laboral </Typography>
      <br />
      <Grid
        className={classes.information}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        {immovables.map((immovable) => (
          <div>
            <Box>
              <Grid
                className={classes.itemListedInfo}
                container
              >
                <ul>
                  <li>
                    <InformationBox info="Se encuentra declarado:">
                      {immovable.isInSunarp ? 'Si' : 'No'}
                    </InformationBox>
                    <InformationBox info="Declaración:">
                      {immovable.isInSunarp ? immovable.sunarpRecord : 'No se encuentra'}
                    </InformationBox>
                    <InformationBox info="Valor:">
                      {immovable.valuation}
                    </InformationBox>
                  </li>
                </ul>
              </Grid>
            </Box>
          </div>
        ))}
      </Grid>
    </div>
  );
};

export default ImmovablesPanel;
