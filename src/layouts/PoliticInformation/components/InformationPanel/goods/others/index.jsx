import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import InformationBox from '../../utils/InformationBox';
import useStyles from '../../utils/styles';

const OthersPanel = (props) => {
  const [others] = React.useState([
    { name: 'el otro', description: 'otros', valuation: 15000.4 },
    { name: 'el otro', description: 'otros', valuation: 1200.422 },
    { name: 'el otro', description: 'otros', valuation: 11002.5 },
    { name: 'el otro', description: 'otros', valuation: 17820.65 },
    { name: 'el otro', description: 'otros', valuation: 110340.99 },
  ]);

  const classes = useStyles();

  return (
    <div className={classes.generalInformation}>
      <Typography variant="h5"> Experiencia Laboral </Typography>
      <br />
      <Grid
        className={classes.information}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        {others.map((other) => (
          <div>
            <Box>
              <Grid
                className={classes.itemListedInfo}
                container
              >
                <ul>
                  <li>
                    <InformationBox info="Nombre:">
                      {other.name}
                    </InformationBox>
                    <InformationBox info="Descripción:">
                      {other.description}
                    </InformationBox>
                    <InformationBox info="Valor:">
                      {other.valuation}
                    </InformationBox>
                  </li>
                </ul>
              </Grid>
            </Box>
          </div>
        ))}
      </Grid>
    </div>
  );
};

export default OthersPanel;
