import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import InformationBox from '../../utils/InformationBox';
import useStyles from '../../utils/styles';

const MovablesPanel = (props) => {
  const [movables] = React.useState([
    { brand: 'hibrido gang', year: '2020', valuation: 15000.4 },
    { brand: 'hibrido gang', year: '2020', valuation: 1200.422 },
    { brand: 'hibrido gang', year: '2020', valuation: 11002.5 },
    { brand: 'hibrido gang', year: '2020', valuation: 17820.65 },
    { brand: 'hibrido gang', year: '2020', valuation: 110340.99 },
  ]);

  const classes = useStyles();

  return (
    <div className={classes.generalInformation}>
      <Typography variant="h5"> Experiencia Laboral </Typography>
      <br />
      <Grid
        className={classes.information}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        {movables.map((movable) => (
          <div>
            <Box>
              <Grid
                className={classes.itemListedInfo}
                container
              >
                <ul>
                  <li>
                    <InformationBox info="Marca:">
                      {movable.brand}
                    </InformationBox>
                    <InformationBox info="Año:">
                      {movable.year}
                    </InformationBox>
                    <InformationBox info="Valor:">
                      {movable.valuation}
                    </InformationBox>
                  </li>
                </ul>
              </Grid>
            </Box>
          </div>
        ))}
      </Grid>
    </div>
  );
};

export default MovablesPanel;
