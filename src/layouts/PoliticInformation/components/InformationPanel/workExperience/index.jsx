import React from 'react';
import Typography from '@material-ui/core/Typography';
import { Box, Grid } from '@material-ui/core';
import InformationBox from '../utils/InformationBox';
import useStyles from '../utils/styles';

const WorkExperiencePanel = (props) => {
  const [workExperiences] = React.useState([
    { company: 'Pontificia Universidad Católica del Perú', profession: 'jefe de practica', startYear: '1998', endYear: '2000' },
    { company: 'Pontificia Universidad Católica del Perú', profession: 'profesor', startYear: '2014', endYear: '2019' },
    { company: 'Pontificia Universidad Católica del Perú', profession: 'rector', startYear: '2018', endYear: '2021' },
    { company: 'Pontificia Universidad Católica del Perú', profession: 'rector', startYear: '2018', endYear: '2021' },
  ]);

  const classes = useStyles();

  return (
    <div className={classes.generalInformation}>
      <Typography variant="h5"> Experiencia Laboral </Typography>
      <br />
      <Grid
        className={classes.information}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        {workExperiences.map((experience) => (
          <div>
            <Box>
              <Grid
                className={classes.itemListedInfo}
                container
              >
                <ul>
                  <li>
                    <InformationBox info="Centro de trabajo:">
                      {experience.company}
                    </InformationBox>
                    <InformationBox info="Ocupación:">
                      {experience.profession}
                    </InformationBox>
                    <InformationBox info="Periodo:">
                      {`${experience.startYear} - ${experience.endYear}`}
                    </InformationBox>
                  </li>
                </ul>
              </Grid>
            </Box>
          </div>
        ))}
      </Grid>
    </div>
  );
};

export default WorkExperiencePanel;
