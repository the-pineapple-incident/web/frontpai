import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  titlePanel: {
    display: 'flex',
    flexDirection: 'row',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  imagenCandidato: {
    width: '250px',
    height: '250px',
    [theme.breakpoints.down('sm')]: {
      width: '150px',
      height: '150px',
    },
  },
  logoPartido: {
    width: '120px',
    height: '120px',
    marginBottom: '10px',
    [theme.breakpoints.down('sm')]: {
      width: '100px',
      height: '100px',
    },
  },
}));

export default useStyles;
