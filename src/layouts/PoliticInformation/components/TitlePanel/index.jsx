import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import useStyles from './styles';

const TitlePanel = (props) => {
  const { imgCandidato, nombreCandidato, puestoCandidato, nombrePartido, imgPartido } = props;
  const classes = useStyles();
  return (
    <div className={classes.titlePanel}>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
      >
        <Grid item xs={12} sm={4} lg={3}>
          <img className={classes.imagenCandidato} src={imgCandidato} alt="candidato" />
        </Grid>
        <div className={classes.container1}>
          <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
            item
            xs={12}
            sm={8}
            lg={9}
          >
            <Typography variant="h4" className="nombre-candidato">
              {nombreCandidato}
            </Typography>
            <Typography variant="h6" className="puesto-candidato">
              {puestoCandidato}
            </Typography>
            <Typography variant="h5">
              {nombrePartido}
            </Typography>
            <img className={classes.logoPartido} src={imgPartido} alt="logo" />
          </Grid>
        </div>
      </Grid>
    </div>
  );
};

export default TitlePanel;
