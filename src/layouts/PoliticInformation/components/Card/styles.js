import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    maxHeight: 600,
    borderWidth: `${1}px`,
    borderStyle: 'solid',
    borderColor: theme.palette.primary.main,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    marginTop: theme.spacing(1),
  },
  button: {
    margin: 'auto',
  },
  title: {
    color: theme.palette.secondary.contrastText,
  },
  chip: {
    //margin: 'auto',
    //display: 'block',
    //marginLeft: 'auto',
    //marginRight: 'auto',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export default useStyles;
