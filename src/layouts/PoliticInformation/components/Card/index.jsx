import React from 'react';
import { Button, Card, CardHeader, CardMedia, CardActions } from '@material-ui/core';
import { useHistory, withRouter } from 'react-router-dom';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import useStyles from './styles';

const CardPoliticParty = (props) => {
  const classes = useStyles();
  const { id, idPerson, name, logo, title, route } = props;
  const history = useHistory();
  const handleClick = () => {
    history.push({
      pathname: route,
      search: `?name=${name}&id=${id}&idPerson=${idPerson}`,
    });
  };
  return (
    <Card className={classes.root} key={id}>
      <CardHeader
        classes={{
          title: classes.title,
        }}
        title={name}
        align="center"
        color="primary"
      />
      <CardMedia
        className={classes.media}
        image={logo}
        title={name}
        height="400"
      />
      {title ?
        (
          <div className={classes.chip}>
            <Chip
              icon={<FaceIcon />}
              label={title}
            />
          </div>
        ) :
        ''}
      <CardActions disableSpacing>
        <Button
          onClick={handleClick}
          variant="contained"
          color="secondary"
          className={classes.button}
        >
          Ver más
        </Button>
      </CardActions>
    </Card>

  );
};

export default withRouter(CardPoliticParty);
