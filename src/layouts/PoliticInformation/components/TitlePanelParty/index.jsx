import React from 'react';
import { Grid, Link, Typography } from '@material-ui/core';
import DescriptionIcon from '@material-ui/icons/Description';
import useStyles from './styles';

const TitlePanelParty = (props) => {
  const { imgParty, nameParty, address, telephones, firstEmail, secondEmail, urlParty, governmentPlan } = props;
  const classes = useStyles();
  function FormColumn() {
    return (
      <>
        <Grid
          container
          direction="column"
          justify="flex-start"
          alignItems="flex-start"
          item
          xs={12}
          sm={8}
          lg={9}
        >
          <Typography variant="h4">{nameParty}</Typography>
          <Typography variant="subtitle1">{address}</Typography>
          <Typography variant="subtitle1">{telephones}</Typography>
          <Typography variant="subtitle1">{firstEmail}</Typography>
          <Typography variant="subtitle1">{secondEmail}</Typography>
          <Link href={urlParty} target="_blank" variant="h6" className={classes.link}>
            {urlParty}
          </Link>
          <Link href={governmentPlan} target="_blank" variant="h6" className={classes.link}>
            <DescriptionIcon className={classes.icon} />
            Plan de gobierno
          </Link>
        </Grid>

      </>
    );
  }

  return (
    <div className={classes.root}>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
      >
        <Grid item xs={12} sm={4} lg={3}>
          <img className={classes.imageParty} src={imgParty} alt={nameParty} />
        </Grid>
        <FormColumn />
      </Grid>
      <br />
    </div>
  );
};

export default TitlePanelParty;
