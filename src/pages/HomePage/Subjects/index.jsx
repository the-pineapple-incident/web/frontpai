import React from 'react';
import Box from '@material-ui/core/Box';
import generalBoxStyle from '../../styles';
import EventCard from '../../../components/EventTopics/lowComponents/EventCard';

const Subjects = (props) => {
  const { subjects = [{ id: 1 }, { id: 2 }] } = props;
  return (
    <Box {...generalBoxStyle}>
      <h1 style={{ color: '#1D3557' }}>Temas</h1>
      <Box display="flex" flexWrap="wrap">
        {subjects.map((subject) => (
          <EventCard key={subject.id} />
        ))}
      </Box>
    </Box>
  );
};

export default Subjects;
