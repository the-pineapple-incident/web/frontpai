import React from 'react';

import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useHistory, withRouter } from 'react-router-dom';
import useStyles from './styles';

function TopicCard(props) {

  const { altTopic, imgTopic, topicTitle, subjectsList } = props;

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(true);

  const handleExpandClick = () => {
    if (window.screen.width < 1024) {
      setExpanded(!expanded);
    }
  };
  const history = useHistory();
  const handleClick = (subject) => {
    history.push({
      pathname: subject.link,
      search: `?name=${subject.name}&id=${subject.id}`,
    });
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={<Avatar alt={altTopic} src={imgTopic} />}
        title={topicTitle}
      />
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent className={classes.ccontent}>
          <div className={classes.container}>
            {subjectsList.map((subject) => {
              return (
                <Button
                  onClick={() => { handleClick(subject); }}
                  variant="contained"
                  color="secondary"
                  className={classes.btn}
                >
                  {subject.name}
                </Button>
              );
            })}
          </div>
        </CardContent>
      </Collapse>
    </Card>
  );
}

export default withRouter(TopicCard);
