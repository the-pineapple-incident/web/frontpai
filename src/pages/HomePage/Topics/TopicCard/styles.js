import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 340,
    maxWidth: 345,
    maxHeight: 400,
    margin: '1rem 2rem',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  ccontent: {
    height: '200px',
    overflowY: 'scroll',
    '&::-webkit-scrollbar': {
      display: 'none',
    },
    '&:hover::-webkit-scrollbar': {
      display: 'block',
    },
    '&:hover::-webkit-scrollbar-thumb': {
      display: 'block',
      backgroundColor: theme.palette.secondary.main,
    },
  },
  container: {
    paddingLeft: '16%',
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  btn: {
    width: '80%',
    backgroundColor: theme.palette.secondary.main,
    padding: '5%',
    margin: '2.5%',
    marginTop: '5',
  },
  lnk: {
    color: 'black',
    width: '100%',
  },
}));

export default useStyles;
