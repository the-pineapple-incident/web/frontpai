import React from 'react';
import { Box, Link, Grid } from '@material-ui/core';
import generalBoxStyle from '../../styles';
import TopicCard from './TopicCard';
import * as ROUTES from '../../../routers/routes';

const subjects = [
  { id: 1, name: 'Tema 1', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 2, name: 'Tema 2', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 3, name: 'Tema 3', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 4, name: 'Tema 4', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 5, name: 'Tema 5', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 6, name: 'Tema 6', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 7, name: 'Tema 7', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
  { id: 8, name: 'Tema 8', link: `${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}` },
];

const topics = [
  { id: 1, altTopic: '', imgTopic: '', topicTitle: 'Tópico 1', subjectsList: subjects },
  { id: 2, altTopic: '', imgTopic: '', topicTitle: 'Tópico 2', subjectsList: subjects },
  { id: 3, altTopic: '', imgTopic: '', topicTitle: 'Tópico 3', subjectsList: subjects },
  { id: 4, altTopic: '', imgTopic: '', topicTitle: 'Tópico 4', subjectsList: subjects },
];

const Topics = (props) => {
  const { urlDimension } = props;
  return (
    <div>
      <Box {...generalBoxStyle}>
        <h1 style={{ color: '#1D3557' }}>Tópicos</h1>
        <Link href={urlDimension} target="_blank" variant="h4">
          Dimensión X
        </Link>
        <br />
        <br />
        <Grid
          container
          spacing={5}
          direction="row"
          alignItems="center"
          justify="space-evenly"
          wrap="nowrap"
        >
          {topics.map((topic) => {
            return (
              <TopicCard
                key={topic.id}
                altTopic={topic.altTopic}
                imgTopic={topic.imgTopic}
                topicTitle={topic.topicTitle}
                subjectsList={topic.subjectsList}
              />
            );
          })}
        </Grid>
        <br />
      </Box>
    </div>
  );
};

export default Topics;
