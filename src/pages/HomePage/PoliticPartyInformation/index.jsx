import React from 'react';
import { Box } from '@material-ui/core';
import TitlePanelParty from '../../../layouts/PoliticInformation/components/TitlePanelParty';
import InformationField from
  '../../../components/CandidateInformation/highComponents/InformationField';
import generalBoxStyle from '../../styles';
import listMenuParty from
  '../../../components/CandidateInformation/highComponents/InformationField/listMenuPoliticParty';

const PoliticPartyInformation = () => {

  return (
    <div>
      <Box {...generalBoxStyle}>
        <TitlePanelParty
          imgParty="https://votoinformado.jne.gob.pe/voto/Resources/imgs/logoop/4.JPG"
          nameParty="Acción Popular"
          address="Av. 9 de Diciembre 218 (Ex Paseo Colón), Lima - Perú"
          telephones="(01) 332-1965 | (01) 423-3204 | #999010056"
          firstEmail="consultasAP@gmai.com"
          secondEmail="consultasAP2@yahoo.es"
          urlParty="https://accionpopular.com.pe/"
          governmentPlan="https://apisije-e.jne.gob.pe/TRAMITE/ESCRITO/1531/ARCHIVO/FIRMADO/4984.PDF"
        />
        <InformationField listMenu={listMenuParty} />
        <br />
        <br />
      </Box>
    </div>
  );
};

export default PoliticPartyInformation;
