import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  box: {
    width: '96%',
  },
}));

export default useStyles;
