import React from 'react';
import Box from '@material-ui/core/Box';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import generalBoxStyle from '../../styles';
import CardPoliticParty from '../../../layouts/PoliticInformation/components/Card';
import * as ROUTES from '../../../routers/routes';
import useStyles from './styles';

const Dimensions = () => {
  const dimensions = [
    { id: 1, name: 'Dimensión Social', logo: '' },
    { id: 2, name: 'Dimensión Económica', logo: '' },
    { id: 3, name: 'Dimensión Institucional', logo: '' },
    { id: 4, name: 'Dimensión Ambiental', logo: '' },
  ];
  const classes = useStyles();

  return (
    <div>
      <Box className={classes.box} {...generalBoxStyle}>
        <h1 style={{ color: '#1D3557' }}>Dimensiones</h1>
        <GridList
          cellHeight="auto"
          cols={2}
          spacing={32}
        >
          {dimensions.map((dimension) => (
            <GridListTile>
              <CardPoliticParty key={dimension.id} id={dimension.id} name={dimension.name} logo={dimension.logo} route={`${ROUTES.HOME_PAGE}${ROUTES.DIMENSION}${ROUTES.TOPIC}`} />
            </GridListTile>
          ))}
        </GridList>
      </Box>

    </div>
  );
};

export default Dimensions;
