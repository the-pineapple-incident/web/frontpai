import React from 'react';
import { Box } from '@material-ui/core';
import { useLocation } from 'react-router-dom';
//import MenuDrawer from '../../../layouts/PoliticInformation/components/MenuDrawer';
import CircularProgress from '@material-ui/core/CircularProgress';
import TitlePanel from '../../../layouts/PoliticInformation/components/TitlePanel';
import InformationField from
  '../../../components/CandidateInformation/highComponents/InformationField';
import generalBoxStyle from '../../styles';
import listMenu from
  '../../../components/CandidateInformation/highComponents/InformationField/listMenu';
import * as services from '../../../services/political-figure/query';

function useQueryRoute() {
  return new URLSearchParams(useLocation().search);
}
const CandidateInformation = () => {
  const queryRoute = useQueryRoute();
  const idPoliticFigure = queryRoute.get('id');
  const idPerson = queryRoute.get('idPerson');
  const {
    loading,
    error,
    data,
  } = useQuery(services.getPersonalInformation, { variables: { idPerson, idPoliticFigure } });
  if (loading) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    );
  };
  if (error) return <p>Error :(</p>;
  console.log(data);

  return (
    <div>
      <Box {...generalBoxStyle}>
        <TitlePanel
          imgCandidato="https://randomuser.me/api/portraits/women/90.jpg"
          nombreCandidato="Yohny Lescano"
          puestoCandidato="Presidente"
          nombrePartido="Acción Popular"
          imgPartido="https://votoinformado.jne.gob.pe/voto/Resources/imgs/logoop/4.JPG"
        />
        <InformationField listMenu={listMenu} />
        <br />
        <br />
      </Box>
    </div>
  );
};

export default CandidateInformation;
