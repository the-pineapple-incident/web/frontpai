import { Box } from '@material-ui/core';
import EventsCards from '../../../components/EventTopics/highComponents/EventsCards';
import generalBoxStyle from '../../styles';

const ParticularEvent = (props) => {
  return (
    <div>
      <Box {...generalBoxStyle}>
        <h1 style={{ color: '#1D3557' }}>Evento particular</h1>
        <EventsCards />
      </Box>
    </div>
  );
};

export default ParticularEvent;
