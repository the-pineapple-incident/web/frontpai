import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
  title: {
    color: theme.palette.secondary.contrastText,
    marginRight: '20px',
    marginBottom: '20px',
  },
}));

export default useStyles;
