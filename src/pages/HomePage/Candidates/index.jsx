import React from 'react';
import { useQuery } from '@apollo/client';
import { Box, Button, Grid, Typography } from '@material-ui/core';
import { useLocation, useHistory } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as services from '../../../services/political-figure/query';
import CardPoliticParty from '../../../layouts/PoliticInformation/components/Card';
import useStyles from './styles';
import generalBoxStyle from '../../styles';
import * as ROUTES from '../../../routers/routes';

function useQueryRoute() {
  return new URLSearchParams(useLocation().search);
}
const Candidates = (props) => {
  const classes = useStyles();
  const queryRoute = useQueryRoute();
  const name = queryRoute.get('name');
  const idParty = queryRoute.get('id');
  const politicalEventId = localStorage.getItem('idPresidenciales');
  const {
    loading,
    error,
    data,
  } = useQuery(services.getAllOfPartyAndEvent, { variables: { idParty, politicalEventId } });
  const history = useHistory();
  if (loading) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    );
  };
  if (error) return <p>Error :(</p>;
  console.log(loading.valueOf, error, data);
  //const { politicParties } = data;
  const handleClick = () => {
    history.push({
      pathname: `${ROUTES.HOME_PAGE}${ROUTES.POLITIC_PARTY_INFORMATION}`,
      search: `?name=${name}`,
    });
  };
  return (
    <div>
      <Box {...generalBoxStyle}>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          <Typography variant="h4" className={classes.title}>{name}</Typography>
          <Button
            onClick={handleClick}
            variant="contained"
            color="secondary"
          >
            Información del Plan de gobierno
          </Button>
        </Grid>
        <br />
        <Grid
          container
          spacing={10}
          direction="row"
          alignItems="center"
          justify="space-evenly"
        >
          { data.politicalFiguresOfPartyAndEvent.map((candidate) => (
            <Grid item xs={12} sm={6} md={3} key={candidate.id}>
              <CardPoliticParty
                key={candidate.id}
                id={candidate.id}
                idPerson={candidate.person.id}
                name={candidate.person.name}
                logo={candidate.person.profilePicture}
                title={candidate.resume.trajectory.position.name}
                route="/elecciones-presidenciales/politico-informacion"
              />
            </Grid>
          ))}
        </Grid>
        <br />
        <br />
      </Box>
    </div>
  );
};

export default Candidates;
