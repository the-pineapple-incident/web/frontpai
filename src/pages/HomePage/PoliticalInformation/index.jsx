import React from 'react';
import { useQuery } from '@apollo/client';
import { Grid, Box, Typography } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import getAllPoliticParties from '../../../services/political-party/query';
import CardPoliticParty from '../../../layouts/PoliticInformation/components/Card';
import SearchField from '../../../components/UI/SearchField';
import * as ROUTES from '../../../routers/routes';
import generalBoxStyle from '../../styles';
import useStyles from './styles';

const removeAccents = (str) => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};
/*const pp = [
  { id: 1, name: 'Acción Popular', logo: '' },
  { id: 2, name: 'Alianza para el progreso', logo: '' },
  { id: 3, name: 'Avanza País', logo: '' },

];*/
const PoliticalInformation = () => {
  const classes = useStyles();
  //obtener el id de presidencial para mandar el query de partidos políticos
  const politicalEventId = localStorage.getItem('idPresidenciales');
  const [search, setSearch] = React.useState(false);
  const [politicParties, setPoliticParties] = React.useState([]);
  const { loading,
    error,
    data } = useQuery(getAllPoliticParties, { variables: { politicalEventId } });
  if (loading) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    );
  };
  if (error) {
    //pp = data.politicalPartiesInEvent;
    console.log(error);
  }

  const webBoxDefault = {
    display: { xs: 'none', sm: 'none', md: 'none', lg: 'block', xl: 'block' },
  };

  const searchPoliticParty = (politicParty) => {
    const tempPoliticParties = [];

    const tempPoliticParty = removeAccents(politicParty);
    if (tempPoliticParty.length > 0) {
      data.politicalPartiesInEvent.map((party) => {
        const tempParty = removeAccents(party.name);
        if (tempParty.toUpperCase().includes(tempPoliticParty.toUpperCase())) {
          tempPoliticParties.push(party);
        }
        return tempPoliticParties;

      });
      setPoliticParties(tempPoliticParties);
      setSearch(true);
      console.log(data.politicalPartiesInEvent, tempPoliticParties);
    } else {
      setSearch(false);
    }

  };

  return (
    <div>
      <Box {...generalBoxStyle}>
        <Box {...webBoxDefault}>
          <Typography variant="h4" align="left" className={classes.title}>Información Política</Typography>
        </Box>
        <br />
        <SearchField clickHandler={searchPoliticParty} />
        <br />
        <br />
        <br />
        <Grid
          container
          spacing={10}
          direction="row"
          alignItems="center"
          justify="space-evenly"
        >

          {search ? politicParties.map((party) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={3}
              key={party.id}
            >
              <CardPoliticParty key={party.id} id={party.id} name={party.name} logo={party.logo} route={`${ROUTES.HOME_PAGE}${ROUTES.CANDIDATES}`} />
            </Grid>
          )) : data.politicalPartiesInEvent.map((party) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={3}
              key={party.id}
            >
              <CardPoliticParty key={party.id} id={party.id} name={party.name} logo={party.logo} route={`${ROUTES.HOME_PAGE}${ROUTES.CANDIDATES}`} />
            </Grid>
          ))}
        </Grid>
        <br />
      </Box>

    </div>
  );
};

export default PoliticalInformation;
