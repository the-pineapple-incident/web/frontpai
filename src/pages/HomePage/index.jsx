import React from 'react';
import {
  Switch,
  Route,
  useRouteMatch,
} from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';
import MainLayout from '../../layouts/MainLayout';
import PoliticalInformation from './PoliticalInformation';
import EventsAndTopics from './Events and Topics';
import SocialMediaAnalysis from './SocialMediaAnalysis';
import Candidates from './Candidates';
import CandidateInformation from './CandidateInformation';
import PoliticPartyInformation from './PoliticPartyInformation';
import Dimensions from './Dimensions';
import Topics from './Topics';
import Subjects from './Subjects';
import NavTabs from '../../layouts/MainLayout/components/NavTabs';
import { navList2 } from '../../layouts/MainLayout/navList';
import * as ROUTES from '../../routers/routes';

import useStyles from './styles';
import ParticularEvent from './ParticularEvent';

const HomePage = () => {
  const { path } = useRouteMatch();
  const phoneBoxDefault = {
    display: { lg: 'none', xl: 'none' },
  };
  const classes = useStyles();
  return (
    <div>
      <MainLayout>
        <Box {...phoneBoxDefault}>
          <Typography variant="h4" align="center" className={classes.title}>Elecciones Presidenciales</Typography>
        </Box>
        <br />
        <NavTabs navList={navList2} />
        <Switch>
          <Route exact path={`${path}`}>
            <PoliticalInformation />
          </Route>
          <Route path={`${path}${ROUTES.CANDIDATES}`} component={Candidates} />
          <Route path={`${path}${ROUTES.CANDIDATE_INFORMATION}`} component={CandidateInformation} />
          <Route path={`${path}${ROUTES.EVENT_TOPIC}`}>
            <EventsAndTopics />
          </Route>
          <Route path={`${ROUTES.PARTICULAR_EVENT}`} component={ParticularEvent} />
          <Route path={`${path}${ROUTES.SOCIAL_MEDIA_ANALYSIS}`} component={SocialMediaAnalysis} />
          <Route path={`${path}${ROUTES.POLITIC_PARTY_INFORMATION}`} component={PoliticPartyInformation} />
          <Route path={`${path}${ROUTES.DIMENSION}${ROUTES.TOPIC}${ROUTES.SUBJECT}`} component={Subjects} />
          <Route path={`${path}${ROUTES.DIMENSION}${ROUTES.TOPIC}`} component={Topics} />
          <Route path={`${path}${ROUTES.DIMENSION}`} component={Dimensions} />
        </Switch>
      </MainLayout>
    </div>
  );
};

export default HomePage;
