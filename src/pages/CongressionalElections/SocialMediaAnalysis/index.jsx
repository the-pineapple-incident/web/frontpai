import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import generalBoxStyle from '../../styles';

const SocialMediaAnalysis = () => {
  return (
    <div>
      <Box {...generalBoxStyle}>
        <h1 style={{ color: '#1D3557' }}>Análisis de Redes Sociales</h1>
        <Typography>Análisis de Redes Sociales</Typography>
      </Box>

    </div>
  );
};

export default SocialMediaAnalysis;
