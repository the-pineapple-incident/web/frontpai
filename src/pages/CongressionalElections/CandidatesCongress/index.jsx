import React from 'react';
import { useQuery } from '@apollo/client';
import { Box, Grid, Typography } from '@material-ui/core';
import { useLocation } from 'react-router-dom';
import getAllPoliticParties from '../../../services/political-party/query';
import CardPoliticParty from '../../../layouts/PoliticInformation/components/Card';
import SelectField from '../../../components/UI/Select';
import useStyles from './styles';
import generalBoxStyle from '../../styles';

function useQueryRoute() {
  return new URLSearchParams(useLocation().search);
}
const CandidatesCongress = (props) => {
  const {
    loading,
    error,
    data,
  } = useQuery(getAllPoliticParties);
  const queryRoute = useQueryRoute();
  const name = queryRoute.get('name');
  console.log(loading.valueOf, error, data);
  const regions = [
    { value: 1, name: 'Amazonas' },
    { value: 2, name: 'Ancash' },
    { value: 3, name: 'Apurimac' },

  ];
  const [region, setRegion] = React.useState();
  const [candidates, setCandidates] = React.useState([]);
  const classes = useStyles();
  const selectRegion = (ev) => {
    setRegion(ev.target.value);
    const regionSelected = ev.target.value;
    const candidatesRegion = [];
    const candidatesAll = [
      { id: 1, name: 'Pablo Secada', logo: '', regionId: 1 },
      { id: 2, name: 'Teddy Cardama', logo: '', regionId: 1 },
      { id: 3, name: 'Mario Guzmán', logo: '', regionId: 1 },
      { id: 4, name: 'Daniel Olivares', logo: '', regionId: 2 },
      { id: 5, name: 'Lucas Talavera', logo: '', regionId: 2 },
      { id: 6, name: 'Galdo Gordon', logo: '', regionId: 2 },
      { id: 7, name: 'Patricia Gamarra', logo: '', regionId: 3 },
      { id: 8, name: 'Rolando Loreno', logo: '', regionId: 3 },
      { id: 9, name: 'Dora Lara', logo: '', regionId: 3 },
    ];
    candidatesAll.map((candidateS) => {
      if (candidateS.regionId === +regionSelected) {
        candidatesRegion.push(candidateS);
      }
      return (candidateS);
    });
    setCandidates(candidatesRegion);

  };

  return (
    <div>
      <Box {...generalBoxStyle}>
        <Typography variant="h4" className={classes.title}>{name}</Typography>
        <br />
        <SelectField
          nameSelect="Región"
          valueSelect={region}
          optionsSelect={regions}
          onChangeHandler={selectRegion}
        />
        <br />
        <Grid
          container
          spacing={2}
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          {candidates.map((candidate) => (
            <Grid item xs={12} sm={6} md={4} key={candidate.id}>
              <CardPoliticParty key={candidate.id} id={candidate.id} name={candidate.name} logo={candidate.logo} route="/elecciones-presidenciales/politico-informacion" />
            </Grid>
          ))}
        </Grid>
      </Box>

    </div>
  );
};

export default CandidatesCongress;
