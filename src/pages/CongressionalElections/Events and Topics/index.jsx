import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import generalBoxStyle from '../../styles';
import MainTimeline from '../../../components/EventTopics/highComponents/MainTimeline';

const EventsAndTopicsCongress = () => {
  return (
    <div>
      <Box {...generalBoxStyle}>
        <h1 style={{ color: '#1D3557' }}>Eventos y temas</h1>
        <Typography>Eventos y temas</Typography>
        <MainTimeline />
      </Box>

    </div>
  );
};

export default EventsAndTopicsCongress;
