import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import CardPoliticParty from '../../../layouts/PoliticInformation/components/Card';
import SearchField from '../../../components/UI/SearchField';
import * as ROUTES from '../../../routers/routes';
import generalBoxStyle from '../../styles';
import useStyles from './styles';

const removeAccents = (str) => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

const PoliticalInformationCongress = (props) => {
  const classes = useStyles();
  const webBoxDefault = {
    display: { xs: 'none', sm: 'none', md: 'none', lg: 'block', xl: 'block' },
  };
  const [politicParties, setPoliticParties] = React.useState([
    { id: 1, name: 'Partido Popular Cristiano', logo: '' },
    { id: 2, name: 'Partido Morado', logo: '' },
    { id: 3, name: 'Fuerza Popular', logo: '' },

  ]);
  const searchPoliticParty = (politicParty) => {
    const tempPoliticParties = [];

    const tempPoliticParty = removeAccents(politicParty);
    politicParties.map((party) => {
      const tempParty = removeAccents(party.name);
      if (tempParty.toUpperCase().includes(tempPoliticParty.toUpperCase())) {
        tempPoliticParties.push(party);
      }
      return tempPoliticParties;

    });

    setPoliticParties(tempPoliticParties);
  };

  return (
    <div>
      <Box {...generalBoxStyle}>
        <Box {...webBoxDefault}>
          <Typography variant="h4" align="left" className={classes.title}>Información Política</Typography>
        </Box>
        <br />
        <SearchField clickHandler={searchPoliticParty} />
        <br />
        <br />
        <br />
        <Grid
          container
          spacing={10}
          direction="row"
          alignItems="center"
          justify="space-evenly"
        >
          {politicParties.map((party) => (
            <Grid item xs={12} sm={6} md={3} key={party.id}>
              <CardPoliticParty key={party.id} id={party.id} name={party.name} logo={party.logo} route={`${ROUTES.CONGRESSIONAL_ELECTIONS}${ROUTES.CANDIDATES}`} />
            </Grid>
          ))}
        </Grid>
        <br />
      </Box>
    </div>
  );
};

export default PoliticalInformationCongress;
