import React from 'react';
import {
  Switch,
  Route,
  useRouteMatch,
} from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';
import MainLayout from '../../layouts/MainLayout';
import PoliticalInformationCongress from './PoliticalInformationCongress';
import CandidatesCongress from './CandidatesCongress';
import NavTabs from '../../layouts/MainLayout/components/NavTabs';
import { navList3 } from '../../layouts/MainLayout/navList';
import * as ROUTES from '../../routers/routes';
import SocialMediaAnalysisCongress from './SocialMediaAnalysis';
import EventsAndTopicsCongress from './Events and Topics';
import useStyles from './styles';

const CongressionalElections = (props) => {
  const { path } = useRouteMatch();
  const phoneBoxDefault = {
    display: { lg: 'none', xl: 'none' },
  };
  const classes = useStyles();
  return (
    <div>
      <MainLayout>
        <Box {...phoneBoxDefault}>
          <Typography variant="h4" align="center" className={classes.title}>Elecciones Congresales</Typography>
        </Box>
        <br />
        <NavTabs navList={navList3} />
        <Switch>
          <Route exact path={`${path}`}>
            <PoliticalInformationCongress />
          </Route>
          <Route path={`${path}${ROUTES.EVENT_TOPIC}`} component={EventsAndTopicsCongress} />
          <Route path={`${path}${ROUTES.SOCIAL_MEDIA_ANALYSIS}`} component={SocialMediaAnalysisCongress} />
          <Route path={`${path}${ROUTES.CANDIDATES}`} component={CandidatesCongress} />
        </Switch>
      </MainLayout>
    </div>

  );
};

export default CongressionalElections;
