/*
* Estilos compartidos entre varios pages
* */

const generalBoxStyle = {
  p: 3,
  border: 1,
  borderColor: 'secondary.main',
};

export default generalBoxStyle;
