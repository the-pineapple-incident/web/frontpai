import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Tabs, Tab, makeStyles } from '@material-ui/core';
import TabPanel from './TabPanel';

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}
const useStyles = makeStyles((theme) => ({
  offset: {
    paddingLeft: theme.spacing(4),

  },
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const NavBar = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className={classes.root}>
      <AppBar position="static" color="primary">
        <Toolbar>
          <Typography variant="h6">
            Elige Informado
          </Typography>
          <Tabs
            className={classes.offset}
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab label="Elecciones Presidenciales" {...a11yProps(0)} />
            <Tab label="Elecciones Congresales" {...a11yProps(1)} />
            <Tab label="Figuras Políticas" {...a11yProps(2)} />
          </Tabs>
        </Toolbar>
      </AppBar>
      <TabPanel value={value} index={0}>
        Elecciones Presidenciales
      </TabPanel>
      <TabPanel value={value} index={1}>
        Elecciones Congresales
      </TabPanel>
      <TabPanel value={value} index={2}>
        Figuras Políticas
      </TabPanel>

    </div>
  );
};

export default NavBar;
