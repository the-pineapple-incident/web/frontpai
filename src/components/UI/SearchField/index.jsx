import React from 'react';
import { Box, Paper } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import useStyles from './styles';

export default function SearchField(props) {
  const { clickHandler } = props;
  const classes = useStyles();
  const [politicParty, setpoliticParty] = React.useState('');

  const searchPoliticParty = (ev) => {
    setpoliticParty(ev.target.value);
  };
  return (
    <div>
      <Box display={{ lg: 'none', xl: 'none' }}>
        <Paper component="form" className={classes.rootMin}>
          <InputBase
            className={classes.input}
            placeholder="Partido Político"
            inputProps={{ 'aria-label': 'Partido Político' }}
            value={politicParty}
            onChange={(ev) => { searchPoliticParty(ev); }}
          />
          <IconButton onClick={() => { clickHandler(politicParty); }} className={classes.iconButton} aria-label="Partido Político">
            <SearchIcon />
          </IconButton>
        </Paper>
      </Box>
      <Box display={{ xs: 'none', sm: 'none', md: 'none', lg: 'block', xl: 'block' }}>
        <Paper component="form" className={classes.root}>
          <InputBase
            className={classes.input}
            placeholder="Partido Político"
            inputProps={{ 'aria-label': 'Partido Político' }}
            value={politicParty}
            onChange={(ev) => { searchPoliticParty(ev); }}
          />
          <IconButton onClick={() => { clickHandler(politicParty); }} className={classes.iconButton} aria-label="Partido Político">
            <SearchIcon />
          </IconButton>
        </Paper>
      </Box>
    </div>

  );
}
