import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import useStyles from './styles';

export default function SelectField(props) {
  const classes = useStyles();
  const { nameSelect, valueSelect, optionsSelect, onChangeHandler } = props;
  /*const [state, setState] = React.useState({
    age: '',
    name: 'hai',
  });

  const handleChange = (event) => {
    const { name } = event.target;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };*/

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="outline-select-simple">{nameSelect}</InputLabel>
        <Select
          native
          value={valueSelect}
          onChange={(ev) => { onChangeHandler(ev); }}
          label={nameSelect}
          inputProps={{
            name: nameSelect,
            id: 'outline-select-simple',
          }}
        >
          <option aria-label="None" value="" />
          {optionsSelect.map((optionSelect) => {
            return (
              <option value={optionSelect.value}>{optionSelect.name}</option>
            );
          })}
        </Select>
      </FormControl>
    </div>
  );
}
