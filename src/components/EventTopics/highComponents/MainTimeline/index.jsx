import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { PARTICULAR_EVENT } from '../../../../routers/routes';
import activeEvents from '../../../../services/active-events/query';
import Timeline from '../../lowComponents/TImeline';
import YearSelector from '../../lowComponents/YearSelector';

const MainTimeline = (props) => {

  const [items, SetItems] = useState([]);

  const {
    loading,
    error = null,
    data = null,
  } = useQuery(activeEvents);

  console.log('====================');
  console.log('items', items);
  console.log('data', data);
  console.log('error', error);
  console.log('loading', loading);
  //Here is gonna be fetched the data to display

  useEffect(() => {
    if (data != null) {
      const aux = data.activeEvents?.map((item) => {
        return {
          id: item.id,
          title: item.period.startDate,
          cardTitle: item.name,
          cardSubtitle: item.description,
          to: `${PARTICULAR_EVENT}`,
        };
      });;
      console.log('************', aux);
      SetItems(aux);
    }
  }, [data]);
  /*
  const items = [
    {
      id: 1,
      title: 'May 1980',
      cardTitle: 'Dunkirk',
      cardSubtitle: 'Men of the British Expeditionary Force (BEF) wade out to..',
      cardDetailedText: 'Men of the British Expeditionary Force (BEF) wade out to..',
      media: {
        type: 'IMAGE',
        source: {
          url: 'https://e.rpp-noticias.io/normal/2016/06/05/202820_159736.jpg',
        },
      },
      to: PARTICULAR_EVENT,
    },
    {
      id: 2,
      title: 'May 1940',
      cardTitle: 'Dunkirk',
      cardSubtitle: 'Men of the British Expeditionary Force (BEF) wade out to..',
      cardDetailedText: 'Men of the British Expeditionary Force (BEF) wade out to..',
      media: {
        name: 'dunkirk beach',
        source: {
          url:
            'https://i2-prod.mirror.co.uk/incoming/article10847802.ece/ALTERNATES/s810/PAY-Dunkirk-in-colour.jpg',
        },
        type: 'IMAGE',
      },
    },
  ];
*/
  return (
    <Box mt={5} display="flex" flexDirection="column" justifyItems="space-around" alignItems="flex-start">
      <YearSelector />
      {error == null && !loading && items.length !== 0 && <Timeline items={items} />}
      {loading && <p>loading...</p>}
    </Box>
  );
};

export default MainTimeline;
