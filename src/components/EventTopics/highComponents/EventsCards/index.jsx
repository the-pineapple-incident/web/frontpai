import React from 'react';
import { Box } from '@material-ui/core';
import EventCard from '../../lowComponents/EventCard';

const EventsCards = (props) => {
  return (
    <Box display="flex" flexWrap="wrap">
      <EventCard />
      <EventCard />
    </Box>
  );
};

export default EventsCards;
