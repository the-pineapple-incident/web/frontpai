import { FormControl, InputLabel, Select } from '@material-ui/core';
import React from 'react';
import useStyles from './styles';

const YearSelector = (props) => {
  const classes = useStyles();
  const { listMenu = [], onClick } = props;
  const [state, setState] = React.useState('');
  const flattedListMenu = [];

  listMenu.forEach((item) => {
    if (item.children === undefined) flattedListMenu.push(item);
    else {
      item.children.forEach((subItem) => {
        flattedListMenu.push(subItem);
      });
    }
  });

  const handleChange = (event) => {
    const { value } = event.target;
    setState(value);
    onClick(value);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel htmlFor="outlined-info">Año</InputLabel>
      <Select
        native
        fullWidth
        value={state}
        onChange={handleChange}
        label="Información"
      >
        <option aria-label="None" value="" />
        {
          flattedListMenu.map((item) => ((
            <option key={item.id} value={item.id}>
              {item.name}
            </option>
          )
          ))
        }
      </Select>
    </FormControl>
  );
};

export default YearSelector;
