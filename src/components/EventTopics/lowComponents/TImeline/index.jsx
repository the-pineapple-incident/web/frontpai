import { Box, Button } from '@material-ui/core';
import React from 'react';
import { Chrono } from 'react-chrono';
import { useHistory } from 'react-router-dom';

const Timeline = (props) => {
  const { items } = props;
  const history = useHistory();

  function handleClick(item) {
    history.push({
      pathname: item.to,
      search: `?event=${item.id}`,
    });
  }

  return (
    <Box h="25vh" width={['auto', 'auto', 'auto', '60rem']}>
      <Chrono
        items={items}
        cardHeight={150}
        mode="VERTICAL"
        theme={{ primary: '#E63946', secondary: 'white' }}
        scrollable
        hideControls
      >
        {items.map((item) => (
          <div key={item.id}>
            <Button onClick={() => handleClick(item)}>
              Ver
            </Button>
          </div>
        ))}
      </Chrono>
    </Box>
  );
};

export default Timeline;
