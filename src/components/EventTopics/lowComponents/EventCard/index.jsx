import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    margin: '1rem 2rem',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

function EventCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={(
          <Avatar aria-label="recipe" className={classes.avatar}>
            A
          </Avatar>
        )}
        /*action={(
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        )}*/
        title="Martín vizcarra"
        subheader="Febrero 15, 2021"
      />
      <CardMedia
        className={classes.media}
        image="https://larepublica.pe/resizer/dVDvbMCF6raViNz9Zncj1qJQqX0=/1250x735/top/smart/cloudfront-us-east-1.images.arcpublishing.com/gruporepublica/EYKEWYOIURDX7BZCXQZOWBZZBM.png"
        title="Martín vizcarra"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sapiente veritatis architecto dolore laboriosam nostrum eos reiciendis commodi quibusdam nobis illo sed odit, cum ab est quasi quisquam hic. Quidem, esse.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Method:</Typography>
          <Typography paragraph>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae obcaecati placeat veritatis!
          </Typography>
          <Typography paragraph>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Libero vitae quidem porro doloribus obcaecati fugit rerum consectetur sed similique, dignissimos quibusdam adipisci voluptatem laudantium doloremque, quod molestiae impedit? Dolorem facere fuga autem beatae nobis commodi unde hic quae laborum voluptatem?
          </Typography>
          <Typography paragraph>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae magnam aperiam iste ratione id! Fugit autem debitis odit quia iusto.
          </Typography>
          <Typography>
            Set aside off of the heat to let rest for 10 minutes, and then serve.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

export default EventCard;
