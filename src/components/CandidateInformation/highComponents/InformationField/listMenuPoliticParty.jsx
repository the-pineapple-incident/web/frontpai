import SocialDimension from '../../../../layouts/PoliticInformation/components/InformationPanelParty/SocialDimension';
import IdeologyInformation from '../../../../layouts/PoliticInformation/components/InformationPanelParty/Ideology';
import VisionInformation from '../../../../layouts/PoliticInformation/components/InformationPanelParty/Vision';

const listMenu = [
  { id: 1, name: 'Ideario', content: <IdeologyInformation /> },
  { id: 2, name: 'Visión', content: <VisionInformation /> },
  { id: 3, name: 'Dimensión Social', content: <SocialDimension /> },
  { id: 4, name: 'Dimensión Económica', content: <SocialDimension /> },
  { id: 5, name: 'Dimensión Ambiental', content: <SocialDimension /> },
  { id: 6, name: 'Dimensión Institucional', content: <SocialDimension /> },
  { id: 7, name: 'Rendición de cuentas', content: <SocialDimension /> },
];

export default listMenu;
