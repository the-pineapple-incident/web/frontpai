import { Hidden } from '@material-ui/core';
import React, { useState } from 'react';
import MiniSideMenu from '../../lowComponents/MiniSideMenu';
import SelectMenu from '../../lowComponents/SelectMenu';
import SideMenuContent from '../../lowComponents/SideMenuContent';
import useStyles from './styles';

const InformationField = (props) => {
  const classes = useStyles();
  const { listMenu } = (props);
  const [infoDisplay, setInfoDisplay] = useState(listMenu[0].content);

  const handleClick = (id = 1) => {
    if (id % 1 !== 0) {
      const splitted = (`${id}`).split('.');
      const realId = parseInt(splitted[0], 10);
      const innerId = parseInt(splitted[1], 10);
      setInfoDisplay(listMenu[realId - 1].children[innerId - 1].content);
    } else setInfoDisplay(listMenu[id - 1].content);
  };

  return (
    <div className={classes.root}>
      <Hidden mdUp implementation="css">
        <SelectMenu listMenu={listMenu} onClick={handleClick} />
      </Hidden>
      <Hidden smDown implementation="css">
        <MiniSideMenu listMenu={listMenu} onClick={handleClick} />
      </Hidden>
      <SideMenuContent>
        {infoDisplay}
      </SideMenuContent>
    </div>
  );
};

export default InformationField;
