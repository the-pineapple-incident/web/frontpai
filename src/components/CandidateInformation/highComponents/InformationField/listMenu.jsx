import InformationPanel from
  '../../../../layouts/PoliticInformation/components/InformationPanel/generalInformation';
import WorkExperiencePanel from
  '../../../../layouts/PoliticInformation/components/InformationPanel/workExperience';
import SentencesPanel from
  '../../../../layouts/PoliticInformation/components/InformationPanel/sentencesList';
import Incomes from
  '../../../../layouts/PoliticInformation/components/InformationPanel/incomes';
import informationList from './InformationList';
import favorityChargeList from './FavorityChargesList';
import EducationPanel from '../../../../layouts/PoliticInformation/components/InformationPanel/education';
import ImmovablesPanel from '../../../../layouts/PoliticInformation/components/InformationPanel/goods/immovables';
import MovablesPanel from '../../../../layouts/PoliticInformation/components/InformationPanel/goods/movables';
import OthersPanel from '../../../../layouts/PoliticInformation/components/InformationPanel/goods/others';

const general =
  (
    <InformationPanel
      title="Datos Generales"
      informationList={informationList}
    />
  );

const cargos =
  (
    <InformationPanel
      title="Cargos Publicos"
      informationList={favorityChargeList}
    />
  );

const listMenu = [
  { id: 1, name: 'Datos generales', content: general },
  { id: 2, name: 'Educación', content: <EducationPanel /> },
  { id: 3, name: 'Experiencia laboral', content: <WorkExperiencePanel /> },
  { id: 4, name: 'Experienca política', content: cargos },
  { id: 5, name: 'Sentencias', content: <SentencesPanel /> },
  { id: 6, name: 'Ingresos', content: <Incomes /> },
  { id: 7,
    name: 'Bienes',
    content: 'aea manito',
    children: [
      { id: 7.1, name: 'Bienes muebles', content: <MovablesPanel /> },
      { id: 7.2, name: 'Bienes inmuebles', content: <ImmovablesPanel /> },
      { id: 7.3, name: 'Otros', content: <OthersPanel /> },
    ] },
  { id: 11, name: 'Información adicional', content: '' },
  { id: 12, name: 'Notas marginales', content: '' },
];

export default listMenu;
