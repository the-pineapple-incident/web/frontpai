const experienceList = [
  { id: '1', info: 'Educado en:', content: 'Universidad Catolica de Santa Maria' },
  { id: '2', info: 'Posgrado:', content: 'Universidad de Chile' },
  { id: '3', info: 'Ocupación:', content: 'Abogado, Político' },
];

export default experienceList;
