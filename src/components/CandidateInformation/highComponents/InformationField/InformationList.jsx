const informationList = [
  { id: '1', info: 'Cargo al que postula:', content: 'Presidente' },
  { id: '2', info: 'DNI:', content: '12345678' },
  { id: '3', info: 'Sexo:', content: 'Masculino' },
  { id: '4', info: 'Fecha de Nacimiento:', content: '01/01/1950' },
  { id: '1R', info: 'Datos de nacimiento', content: '' },
  { id: '2R', info: 'Pais:', content: 'Perú' },
  { id: '3R', info: 'Departamento:', content: 'Lima' },
  { id: '4R', info: 'Provincia:', content: 'Lima' },
  { id: '5R', info: 'Distrito:', content: 'San Miguel' },
];

export default informationList;
