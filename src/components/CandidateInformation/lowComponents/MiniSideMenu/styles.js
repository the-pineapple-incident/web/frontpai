import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    minWidth: 250,
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  subItem: {
    paddingLeft: 40,
  },
}));

export default useStyles;
