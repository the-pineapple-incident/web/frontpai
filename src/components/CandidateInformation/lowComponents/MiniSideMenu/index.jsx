import React from 'react';
import List from '@material-ui/core/List';
import Collapse from '@material-ui/core/Collapse';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Paper } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import useStyles from './styles';

const MiniSideMenu = (props) => {
  const classes = useStyles();
  const { listMenu = [], onClick } = props;
  const [open, setOpen] = React.useState(false);
  console.log(listMenu[0].children);

  const handleChildrenClick = () => {
    setOpen(!open);
  };

  return (
    <Paper>
      <div className={classes.root}>
        <List component="nav" aria-label="mini menu">
          {listMenu.map((item) => (
            item.children === undefined ? (
              <ListItem key={item.id} button onClick={() => onClick(item.id)}>
                {item.icon && (
                  <ListItemIcon>
                    {item.icon}
                  </ListItemIcon>
                )}
                <ListItemText primary={item.name} />
              </ListItem>
            ) : (
              <div>
                <ListItem key={item.id} button onClick={() => handleChildrenClick()}>
                  {item.icon && (
                    <ListItemIcon>
                      {item.icon}
                    </ListItemIcon>
                  )}
                  <ListItemText primary={`${item.name}`} />
                  {open ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                  <List disablePadding>
                    {item.children.map((subItem) => {
                      return (
                        <ListItem key={subItem.id} button onClick={() => onClick(subItem.id)} className={classes.subItem}>
                          {subItem.icon && (
                            <ListItemIcon>
                              {subItem.icon}
                            </ListItemIcon>
                          )}
                          <ListItemText primary={subItem.name} />
                        </ListItem>
                      );
                    })}
                  </List>
                </Collapse>
              </div>
            )
          ))}
        </List>
      </div>
    </Paper>
  );
};

export default MiniSideMenu;
