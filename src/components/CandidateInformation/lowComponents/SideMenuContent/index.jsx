import { Box } from '@material-ui/core';
import React from 'react';

const SideMenuContent = (props) => {
  const { children } = props;

  return (
    <Box p={2} w={1}>
      {children}
    </Box>
  );
};

export default SideMenuContent;
