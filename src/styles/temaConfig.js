import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#E63946',
      contrastText: '#F1FAEE',
    },
    secondary: {
      main: '#A8DADC',
      contrastText: '#1D3557',
    },
  },
});

export default theme;
