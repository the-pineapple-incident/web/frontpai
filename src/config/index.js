const config = {
  app: {
    name: process.env.REACT_APP_PUBLIC_APP_NAME,
  },
  api: {
    baseUrl: process.env.REACT_APP_API_HOST_URL,
  },
};

export default config;
