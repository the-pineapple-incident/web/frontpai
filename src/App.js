import * as dotenv from 'dotenv';
import { ThemeProvider } from '@material-ui/core/styles';
import { ApolloProvider } from '@apollo/client';
import theme from './styles/temaConfig';
import AppRouter from './routers/AppRouter';
import client from './services/index';

dotenv.config();

function App() {
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <AppRouter />
      </ThemeProvider>
    </ApolloProvider>

  );
}

export default App;
