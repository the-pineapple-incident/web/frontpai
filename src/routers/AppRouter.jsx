import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
} from 'react-router-dom';

import * as ROUTES from './routes';

import PublicRoute from './route-wrapers/PublicRoute';
import CongressionalElections from '../pages/CongressionalElections';
import HomePage from '../pages/HomePage/index';
import PoliticalFigures from '../pages/PoliticalFigures';

const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <PublicRoute
          component={HomePage}
          path={ROUTES.HOME_PAGE}
        />
        <PublicRoute
          component={CongressionalElections}
          path={ROUTES.CONGRESSIONAL_ELECTIONS}
        />
        <PublicRoute
          component={PoliticalFigures}
          path={ROUTES.POLITICAL_FIGURES}
        />
      </Switch>
      <Redirect from="/" to={ROUTES.HOME_PAGE} />
    </Router>
  );
};

export default AppRouter;
