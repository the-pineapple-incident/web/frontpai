import React from 'react';
import { Route } from 'react-router-dom';

import { HOME_PAGE } from '../routes';

const PublicRoute = (props) => {
  const { component, path = HOME_PAGE, ...rest } = props;

  return (
    <Route
      component={component}
      path={path}
      {...rest}
    />
  );
};

export default PublicRoute;
