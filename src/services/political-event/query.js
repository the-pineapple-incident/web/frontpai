import { gql } from '@apollo/client';

const getAllActivePoliticalEvents = gql`
  query {
    activePoliticalEvents {
      id
      name
      state
      year
    }
  }
`;

export default getAllActivePoliticalEvents;
