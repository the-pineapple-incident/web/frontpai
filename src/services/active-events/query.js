import { gql } from '@apollo/client';

const activeEvents = gql`
  query {
    activeEvents {
      id
      name
      description
      period {
        startDate
        endDate
      }
      keywords
    }
  }
`;

export default activeEvents;
