import { ApolloClient, InMemoryCache, from, HttpLink } from '@apollo/client';
import config from '../config';

const link = from([
  new HttpLink({ uri: config.api.baseUrl }),
]);

const client = new ApolloClient(
  {
    cache: new InMemoryCache(),
    link,
  },
);

export default client;
