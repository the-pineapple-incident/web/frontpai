import { gql } from '@apollo/client';

export const getAllOfPartyAndEvent = gql`
  query($idParty: ID, $idEvent: ID) {
    politicalFiguresOfPartyAndEvent(idParty: $idParty, idEvent: $idEvent) {
      id
      person {
        id
        name
        profilePicture
        resume {
          trajectory {
            position {
              id
              name
            }
          }
        }
      }
    }
  }
`;

export const getPersonalInformation = gql`
  query($personId: ID, $politicalFigureId: ID) {
    politicalFigure(
      personId: $personID
      politicalFigureId: $politicalFigureId
    ) {
      id
      candidacy {
        politicalParty {
          id
          name
          logo
        }
        applicationPlace {
          id
          code
          name
        }
      }
      reported {
        incomes {
          year
          private {
            netIncomes
            rent
            others
            total
          }
          public {
            netIncomes
            rent
            others
            total
          }
          netIncomes
          rent
          others
          total
        }
        mistakeAnnotations {
          annotationNumber
          recordNumber
          reportNumber
          documentNumber
          currentContent
          correctedContent
        }
        additionalInfo
        goods {
          movables {
            vehicle
            brand
            license
            model
            year
            characteristics
            valuation
          }
          inmovables {
            type
            placeId
            isInSunarp
            sunarpRecord
            valuation
          }
          others {
            name
            description
            characteristics
            valuation
          }
        }
      }
      person {
        id
        name
        firstLastname
        secondLastname
        gender
        profilePicture
        birth {
          date
          place {
            id
            name
          }
        }
        dwelling {
          address
          place {
            id
            name
          }
        }
        identity {
          name
          code
        }
        judgment {
          civil {
            sentence
            record
            judicialAuthority
            verdict
          }
          criminal {
            record
            sentenceDate
            judicialAuthority
            penalCrime
            verdict
            modality
          }
        }
        resume {
          education {
            level
            completed
            institution
            details {
              specialty
              bachelorYear
              graduateYear
              degree
            }
          }
          experience {
            company
            place
            profession
            startYear
            endYear
          }
          trajectory {
            position {
              name
              positionType
              electionState
              politicalParty {
                name
              }
              period {
                startYear
                endYear
              }
            }
          }
          waivers {
            politicalParty {
              name
            }
            date
          }
        }
      }
    }
  }
`;
