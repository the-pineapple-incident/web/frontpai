import { gql } from '@apollo/client';

const getAllPoliticalParties = gql`
  query($politicalEventId: ID!) {
    politicalPartiesInEvent(politicalEventId: $politicalEventId) {
      id
      name
      logo
    }
  }
`;

export default getAllPoliticalParties;
